# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).


## [1.0.1] - 2016-11-18 - _Dawson_
### Changed
- Updated Logging Path
- Changed Default App Environment Variable Name, is not used however.


## [1.0.0] - 2016-11-18 - _Imperial_
### Added
- Initial Release of VCSAT Unsubscribe Server
