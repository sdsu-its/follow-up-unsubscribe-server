VCSAT Unsubscribe Server
========================

For tracking our Virtual Support Customer Satisfaction, we send emails prompting
users for feedback. However, to be CAN-SPAM compliment, all emails need an option
to unsubscribe from Emails. This server fulfills that purpose.

## Set-Up
### Desk.com
You will need to create a customer user field, with key `vcsat_unsub`, which
will be an **True/False** question. If this option is true, they will NOT
receive any emails prompting them for feedback.

You will a desk.com user who has the necessary permissions to modify users,
preferable with a password that does not contain any symbols, as that is how
we will communicate with the API. See the Vault configuration below for they
necessary keys & secrets that we will need to configure.

### Vault
You will need to create a secret, `desk`, in the vault. Information on how to
setup Vault and AppRoles can be found at:
https://sdsu-its.gitbooks.io/vault/content/

You will need to set the `VAULT_ADDR`, `VAULT_ROLE` and `VAULT_SECRET` environment variables to their corresponding values.

#### Keys
- `username` - API User's Username
- `password` - API User's Password
- `site_name` - Your Desk.com Site Name
