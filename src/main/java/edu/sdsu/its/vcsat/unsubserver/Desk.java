package edu.sdsu.its.vcsat.unsubserver;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import edu.sdsu.its.vcsat.unsubserver.Models.Customer;
import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Desk.com API Interface.
 *
 * A custom User Field (vcsat_unsub) is set to track whether or not a user is subscribed to follow up messages.
 *
 * @author Tom Paulus
 *         Created on 10/28/16.
 */
public class Desk {
    private static final Logger LOGGER = Logger.getLogger(Desk.class);

    private static final String SITE_NAME = Vault.getParam("desk", "site_name");
    private static final String USERNAME = Vault.getParam("desk", "username");
    private static final String PASSWORD = Vault.getParam("desk", "password");

    /**
     * Update a User's object, all fields that are set will be updated.
     *
     * @param customer {@link Object} Updated Customer Object
     * @return {@link Customer} Customer Object
     */
    public static Customer updateCustomer(final Customer customer) {
        URI uri;
        try {
            uri = new URIBuilder()
                    .setScheme("https")
                    .setHost(SITE_NAME + ".desk.com")
                    .setPath("/api/v2/customers/" + customer.getId())
                    .build();
        } catch (URISyntaxException e) {
            LOGGER.warn("Problem Generating Update URL", e);
            return null;
        }

        Gson gson = new Gson();
        final HttpResponse response = patch(uri, gson.toJson(customer));

        if (response.getStatus() != 200) {
            LOGGER.warn(String.format("Could not find User with ID: %d", customer.getId()));
        }

        return gson.fromJson(response.getBody().toString(), Customer.class);
    }

    /**
     * Retrieve a customer object from the Desk API via the customer's email.
     *
     * @param email {@link String} customer's email
     * @return {@link Customer} Customer Object
     */
    public static Customer getCustomerByEmail(final String email) {
        URI uri;
        try {
            uri = new URIBuilder()
                    .setScheme("https")
                    .setHost(SITE_NAME + ".desk.com")
                    .setPath("/api/v2/customers/search")
                    .setParameter("email", email)
                    .build();
        } catch (URISyntaxException e) {
            LOGGER.warn("Problem Generating Search Query URL", e);
            return null;
        }

        Gson gson = new Gson();
        final HttpResponse response = get(uri);
        if (response.getStatus() != 200) {
            LOGGER.warn(String.format("Problem Retrieving User by Email Address - HTTP Code: %d", response.getStatus()));
            return null;
        }
        SearchResult result = gson.fromJson(response.getBody().toString(), SearchResult.class);

        if (result.total_entries < 1 || result._embedded.entries.length < 1) {
            return null;
        }

        return result._embedded.entries[0];
    }

    /**
     * Make HTTP Get requests and return the Response form the Server.
     *
     * @param uri {@link URI} URI used to make get Request.
     * @return {@link HttpResponse} Response from get Request.
     */
    private static HttpResponse get(final URI uri) {
        LOGGER.info("Making a get request to: " + uri.toString());

        HttpResponse<String> response = null;

        try {
            response = Unirest.get(uri.toString())
                    .header("accept", "application/json")
                    .basicAuth(USERNAME, PASSWORD)
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return response;
    }

    /**
     * Send a PATCH Request with Basic Authentication for MailChimp
     *
     * @param uri     {@link URI} URI to PATCH to.
     * @param payload {@link String} Payload to PATCH
     * @return {@link HttpResponse} Response from the MailChimp Server
     */
    private static HttpResponse patch(final URI uri, final String payload) {
        LOGGER.debug(String.format("Making a PATCH request to %s", uri.toString()));

        HttpResponse<String> response = null;

        try {
            response = Unirest.patch(uri.toString())
                    .header("accept", "application/json")
                    .basicAuth(USERNAME, PASSWORD)
                    .body(payload)
                    .asString();
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return response;
    }

    private static class SearchResult {
        int total_entries;
        Data _embedded;

        private static class Data {
            Customer[] entries;
        }

    }
}
