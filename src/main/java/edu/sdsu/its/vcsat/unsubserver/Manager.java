package edu.sdsu.its.vcsat.unsubserver;

import com.google.gson.Gson;
import edu.sdsu.its.vcsat.unsubserver.Models.Customer;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * These endpoints allow the user to unsubscribe from emails that they receive as a result of the VCSAT
 * Follow Up Emails.
 *
 * @author Tom Paulus
 *         Created on 10/28/16.
 */
@Path("/")
public class Manager {
    private static final Logger LOGGER = Logger.getLogger(Manager.class);
    private static final Gson GSON = new Gson();

    /**
     * In order to comply with CAN-SPAM Regulations, all emails must contain an unsubscribe link.
     * Update the Database to reflect this request.
     *
     * @param id {@link Integer} Customer to be unsubscribed
     * @return {@link Response} {@see Models.User} User associated to the provided email
     */
    @Path("unsubscribe")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response unsubscribe(@QueryParam("id") final Integer id) {
        LOGGER.info("Recieved Request: [GET] UNSUBSCRIBE - id = " + id);
        Response response;

        if (id == null || id == 0) {
            return Response.status(Response.Status.PRECONDITION_FAILED).entity(GSON.toJson(new SimpleMessage("No ID included in request"))).build();
        }

        Customer customer = new Customer(id);
        customer.custom_fields.put("vcsat_unsub", true);

        customer = Desk.updateCustomer(customer);

        if (customer != null) {
            LOGGER.debug(String.format("Updated User with ID: %d - Has been Unsubscribed", id));
            response = Response.status(Response.Status.ACCEPTED).entity(GSON.toJson(customer)).build();

        } else {
            LOGGER.warn(String.format("No Matching User found for id: %d", id));
            response = Response.status(Response.Status.NOT_FOUND).entity(GSON.toJson(new SimpleMessage("User Not Found"))).build();
        }
        return response;
    }

    /**
     * Endpoint to which the resubscribe form is POSTed on the Unsubscribe page in case the user accidentally unsubscribed.
     *
     * @param email {@link String} Email Address to be Subscribed
     * @return {@link Response} {@see Models.User} User associated to the provided email
     */
    @Path("subscribe")
    @POST
    @Consumes(MediaType.WILDCARD)
    @Produces(MediaType.APPLICATION_JSON)
    public Response subscribe(@QueryParam("email") final String email) {
        LOGGER.info("Recieved Request: [GET] SUBSCRIBE - email = " + email);
        Response response;

        if (email != null && email.length() < 0) {
            return Response.status(Response.Status.PRECONDITION_FAILED).entity(GSON.toJson(new SimpleMessage("No Email included in request"))).build();
        }

        Customer customer = Desk.getCustomerByEmail(email);

        if (customer != null) {
            LOGGER.info(String.format("Resubscribed Customer with id: %d to VCSAT Emails", customer.getId()));
            customer.custom_fields.put("vcsat_unsub", false);
            customer = Desk.updateCustomer(customer);
            response = Response.status(Response.Status.ACCEPTED).entity(GSON.toJson(customer)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).entity(GSON.toJson(new SimpleMessage("User not Found"))).build();
        }

        return response;
    }


    private static class SimpleMessage {
        String message;

        private SimpleMessage(String message) {
            this.message = message;
        }
    }
}