package edu.sdsu.its.vcsat.unsubserver.Models;

import java.util.HashMap;

/**
 * Models a Desk.com Customer
 *
 * @author Tom Paulus
 *         Created on 10/28/16.
 */
public class Customer {
    public int id;
    public String first_name;
    public String last_name;
    public HashMap<String, Object> custom_fields = new HashMap<>();

    public Customer(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", custom_fields=" + custom_fields +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (id != customer.id) return false;
        if (first_name != null ? !first_name.equals(customer.first_name) : customer.first_name != null) return false;
        if (last_name != null ? !last_name.equals(customer.last_name) : customer.last_name != null) return false;
        return custom_fields != null ? custom_fields.equals(customer.custom_fields) : customer.custom_fields == null;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
