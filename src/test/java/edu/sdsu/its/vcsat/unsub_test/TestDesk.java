package edu.sdsu.its.vcsat.unsub_test;

import edu.sdsu.its.vcsat.unsubserver.Desk;
import edu.sdsu.its.vcsat.unsubserver.Models.Customer;
import org.apache.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test Desk.com API Handler
 *
 * @author Tom Paulus
 *         Created on 11/8/16.
 */
public class TestDesk {
    private static final Logger LOGGER = Logger.getLogger(TestDesk.class);

    private static final String TEST_USER_EMAIL = "example@nobody.sdsu.edu";
    private static final String UNSUB_FIELD = "vcsat_unsub";

    @Test
    public void getUser() throws Exception {
        LOGGER.info(String.format("Retrieving User with Email: \"%s\"", TEST_USER_EMAIL));
        Customer customer = Desk.getCustomerByEmail(TEST_USER_EMAIL);
        assertTrue("Customer with Sample Email Not Found", customer != null);
        LOGGER.debug("Retrieved Customer:\n" + customer.toString());

        assertTrue("Customer has no Name",
                customer.first_name != null &&
                        !customer.first_name.isEmpty() &&
                        customer.last_name != null &&
                        !customer.last_name.isEmpty());

        assertNotEquals("Customer has no ID", customer.id, 0);
    }

    @Test
    public void updateUser() throws Exception {
        Customer customer = Desk.getCustomerByEmail(TEST_USER_EMAIL);
        assertTrue("Customer with Sample Email Not Found", customer != null);

        assertTrue("Unsub Field does not exist", customer.custom_fields.containsKey(UNSUB_FIELD));
        final Boolean currentValue = (Boolean) customer.custom_fields.get(UNSUB_FIELD);
        LOGGER.debug(String.format("Current Field Value is %s", currentValue == null ? "null" : currentValue.toString()));

        boolean newValue;

        if (currentValue == null || !currentValue) {
            LOGGER.info("Current Value is either not set, or set to False, Setting to True.");
            newValue = true;
        } else {
            LOGGER.info("Current Value is to True, setting to False.");
            newValue = false;
        }

        LOGGER.debug("Updating Customer");
        customer.custom_fields.put(UNSUB_FIELD, newValue);
        Customer updatedCustomer = Desk.updateCustomer(customer);

        assertEquals("Update did not return the Updated Customer", updatedCustomer, customer);

        assertEquals("API did not Update", updatedCustomer, Desk.getCustomerByEmail(TEST_USER_EMAIL));
    }
}
